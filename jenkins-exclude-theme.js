var cssUrlPath = '/dist/material-cyan.css';
var urlPaths = ['/view/Package%20Monitor/', '/view/Acceptance%20Monitor/'];

function locationMatches() {
  for (var i = 0; i < urlPaths.length; i++) {
    if (window.location.href.endsWith(urlPaths[i])) {
      return true;
    }
  }
    
  return false;
}

if (locationMatches()) {
  var links = document.getElementsByTagName("link");
  for (var i = 0; i < links.length; i++) {
  	var link = links[i];
    if (link.href.endsWith(cssUrlPath)) {
    	link.href = '';
    }
  }
}